variable "PROJECT_OWNER" {
  type = string
}
variable "VPC_NAME" {
  type = string
}
variable "VPC_CIDR" {
  type = string
}
variable "DNS_HOSTNAME" {
  type = bool
  default = true
}
variable "PUBLIC_CIDR" {
  type = string
}
variable "AWS_REGION" {
  type = string
}
variable "AWS_ZONES" {
  type = list(string)
}
variable "PUBLIC_SUBNET_CIDR" {
  type = list(string)
}
variable "PRIVATE_SUBNET_CIDR" {
  type = list(string)
}
variable "ECR_PULL_ACCESS_ROLE" {
  type = string
}