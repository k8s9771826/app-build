# Create VPC
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.8.1"

  name = var.VPC_NAME
  cidr = var.VPC_CIDR

  azs             = var.AWS_ZONES
  private_subnets = var.PRIVATE_SUBNET_CIDR
  public_subnets  = var.PUBLIC_SUBNET_CIDR

  enable_nat_gateway = true
}


# Create EKS Cluster
module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "20.15.0"
  cluster_name    = "beautiful-land"
  cluster_version = "1.30"

 

  subnet_ids = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id

  eks_managed_node_group_defaults = {
    instance_types = ["t3.medium"]
  }

  eks_managed_node_groups  = {
    iam_role_arn = "arn:aws:iam::245354769597:role/ecr_pull_access"
    eks_nodes = {
      desired_size     = 2
      max_size         = 2
      min_size         = 2

      instance_type = ["t3.medium"]
      key_name      = "my-eks-key"
    }
  }
}

module "ecr" {
  source = "terraform-aws-modules/ecr/aws"
  version = "2.2.1"

  repository_name = "beautiful-land-images"
  repository_image_tag_mutability = "MUTABLE"
  repository_lifecycle_policy = jsonencode({
    rules = [
      {
        rulePriority = 1,
        description  = "Keep last 30 images",
        selection = {
          tagStatus     = "tagged",
          tagPrefixList = ["v"],
          countType     = "imageCountMoreThan",
          countNumber   = 30
        },
        action = {
          type = "expire"
        }
      }
    ]
  })
}